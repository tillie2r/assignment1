#include "Function.h"
struct Complex
{
	float Real;
	float Imaginary;
};
int main()
{
	bool quit = false;
	string input_string;
	Complex number1;
	Complex number2;
	string command, token;
	cout << "Enter a command (A S D M) followed by two complex pairs to start. Press Q to quit " << endl;
	while (quit == false)
	{
		
		getline(std::cin, input_string);

		istringstream to_parse(input_string);
		getline(to_parse, command, ' ');
			// convert string to components
		getline(to_parse, token, ' ');
		number1.Real = stof(token);
		getline(to_parse, token, ' ');
		number1.Imaginary = stof(token);

		getline(to_parse, token, ' ');
		number2.Real = stof(token);
		getline(to_parse, token, ' ');
		number2.Imaginary = stof(token);
		// set loop till command is q
		if (command == "q" || command == "Q")
			quit = true;
		else
			print(math(command, number1, number2));
		

	}

	return 0;
}