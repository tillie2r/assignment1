/**
Along with Source.cpp and function.cpp this program is for submission to assignment 1.
Program constructs complex number class and allows basic math operations to be performed
by stating a command and providing to sets of float values. Input expected is a string with 
the format "char float float float float" any other input should generate and error message
that will be displayed.  tested with file Data.txt to check if works with file input. 
*/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

struct Complex;
Complex add(Complex, Complex);
Complex sub(Complex, Complex);
Complex div(Complex, Complex);
Complex mul(Complex, Complex);
Complex math(string, Complex, Complex);
float abs(float);
float stof(string);
void print(Complex);