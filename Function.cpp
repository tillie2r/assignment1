#include "Function.h"
// set complex number data type
struct Complex
{
	float Real;
	float Imaginary;
};
// printing function
void print(Complex Value)
{
	string sign = " + ";
	if (Value.Imaginary < 0)
		sign = " - ";
	cout << Value.Real << sign + " J" << abs(Value.Imaginary) << endl;

}
 // to compile in g++ takes absolute value of passed float remopve to ompile in visual studio
float abs(float value)
{
	if (value < 0)
		return value*-1;
	else
		return value;
}
// to compile in g++ converts string to float remove to compile in visual studio
float stof(string str)
{
	istringstream ss(str);
	float value;

	ss >> value;
	if (!ss) 
	{
		std::cerr << "error converting provided value: " << str << 
'\n';
		return 1;
	}
}
// add complec numbers
Complex add(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real + Variable2.Real;
	equals.Imaginary = Variable1.Imaginary + Variable2.Imaginary;
	return equals;
}
// subtract complex numbers
Complex sub(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real - Variable2.Real;
	equals.Imaginary = Variable1.Imaginary - Variable2.Imaginary;
	return equals;
}
// multiply complex numbers
Complex mul(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real*Variable2.Real - Variable1.Imaginary*Variable2.Imaginary;
	equals.Imaginary = Variable1.Real*Variable2.Imaginary + Variable2.Real*Variable1.Imaginary;
	return equals;
}
// divide complex numbers
Complex div(Complex Variable1, Complex Variable2)
{
	Complex equals;
	float Bottom = Variable2.Real*Variable2.Real + Variable2.Imaginary*Variable2.Imaginary;

	equals.Real = (Variable1.Real*Variable2.Real + Variable1.Imaginary*Variable2.Imaginary) / Bottom;  // Check this line for sign error
	equals.Imaginary = (-Variable1.Real*Variable2.Imaginary + Variable2.Real*Variable1.Imaginary) / Bottom;

	return equals;
}
//Case statement to select command function. catch outputs error
Complex math(string com, Complex Variable1, Complex Variable2)
{
	Complex result;
	char command = com[0];
	result.Real = 0;
	result.Imaginary = 0;
	if (com.length() > 1)
		command = 'q';
	switch (command)
	{
	case 'a':
	case 'A':
		result = add(Variable1, Variable2);
		break;

	case 'm':
	case 'M':
		result = mul(Variable1, Variable2);
		break;

	case 'S':
	case 's':
		result = sub(Variable1, Variable2);
		break;

	case 'D':
	case 'd':
		result = div(Variable1, Variable2);
		break;
	case 'q':
	case 'Q':
	default:
		cout << "Error in command" << endl;
	}
	return result;

}
